FROM debian:buster-slim
COPY --from=balthek/zola:0.14.0 /usr/bin/zola /usr/bin/zola
ADD . .
